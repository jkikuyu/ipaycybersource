<!DOCTYPE html>
<html>
<head>
    
<script src="https://songbird.cardinalcommerce.com/cardinalcruise/v1/songbird.js"></script>

<script src="assets/restcc.js"></script>

</head>
<?php
    //$data= file_get_contents('php://input');
    $ccnum = $_POST['ccnum'];

    $ccode = $_POST['cust_Country'];
    $cardType = (substr($ccnum,0,1)==='4')?'001':'002';
    $curr = "KES";
    $city = "Nairobi";
    $orderNo = $_POST['order_id'];
    $origin = $_POST['origin'];
     
    switch($ccode){
        case 'US':
            $curr = "USD";
            break;
        case 'UG':
            $curr = "UGX";
            $city = "Kampala";
            
            break;
        case 'TZ':
            $curr = "TZS";  
            $city = "Darasalem";

            break;
        default:
            $curr = "KES";
            $city="Nairobi";
         
    }
    if($ccode ===$origin){} 
    else{
        switch($origin){
            case 'UG':
                $city = "Kampala";
                $ccode = "UG";

                break;
            case 'TZ':
                $city = "Darasalem";
                $ccode = "TZ";

                break;
            default:
                $city="Nairobi";
                $ccode = "KE";

        }
    }

    $ccData = '{
    "cardType": "'. $cardType.'",
    "street": "'. $_POST['street'].'",
    "OrderDetails": {
        "OrderNumber":"'. $_POST['order_id'].'",
        "OrderDescription": "Test Purchase" ,
        "Amount": "'.$_POST['total'].'",
        "CurrencyCode":"'. $curr.'",
        "OrderChannel": "M",
        "TransactionId":"'.uniqid().'"
    },
    "Consumer": {
        "Email1": "'.$_POST['email'].'",
        "BillingAddress": {
            "FirstName": "'.$_POST['cust_fname'].'",
            "MiddleName": "",
            "LastName": "'.$_POST['cust_lname'].'",
            "Address1": "'. $_POST['street'].'",
            "City":  "'.$city.'",
            "CountryCode": "'.$ccode.'",
            "Phone1": "'.$_POST['phone'].'"
        }
    },
    "Account": {
        "AccountNumber": "'.$_POST['ccnum'].'",
        "CardCode":  "'.$_POST['cvv'].'",
        "ExpirationMonth":  "'.$_POST['ccmonth'].'",
        "ExpirationYear": "20'.$_POST['ccyear'].'"
    }
}';

?>
<!--
<script src="js/restcc.js"></script>
-->
    


 <meta charset="utf-8"/>
<body>
    <p> Please wait while processing ......</p>
    <div id="mess"></div>
<script>      
    var cc = <?php echo $ccData; ?>;
    
    var cardData=JSON.stringify(cc);
		//console.log(cardData);
    try {
        let sc = new SecureCard(cardData);

        sc.enable();
    }catch(err) {
        console.log(err);
    }


</script>
</body>
</html>