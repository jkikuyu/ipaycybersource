# Cybersource Integration
This is a sample integration that launches a payment page. Card details keyed in are forwarded to cybersource for processing. 


## Credentials Required

A .env file is located in classes/secure folder ensure you enter the required credentials
```
#Cybersource Credentials
MERCHANT_ID_US=""

TRANSACTION_KEY_US = ""
#Cardinal Credentials
ORGUNIT_ID= 
API_ID =  
API_KEY= 
```
## Launching the sample application.
Place the application in your webserver folder e.g `/var/public/html` and launch in your browser eg. `http://localhost/ipaycybersource`.

The page will resemble this one here
![iPay Payment Page](assets/ppage.png)

## Viewing results
The browser developer mode will assist in establishing the outcome of transaction processing. A successful enrollment check will display the otp page. A successful payment authentication will result in deduction being effected in customer account and entries in cybersource ebc.


